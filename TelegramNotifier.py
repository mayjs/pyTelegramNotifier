import telegram
import json
import os

class TelegramNotifier:
	def __init__(self,config=os.path.join(os.path.dirname(__file__),"config.json")):
		with open(config) as config_file:
			config = json.load(config_file)
			self.bot = telegram.Bot(config["bot"]["token"])
			self.chats = config["chats"]
 
	# Returns the number of chats loaded
	def numChats(self):
		return len(self.chats)

	# Send a given message to all clients
	def sendMessageToAll(self,message):
		for chat in self.chats:
			self.bot.sendMessage(chat_id=chat["id"],text=message)

###########

if __name__ == "__main__":
	import sys

	if len(sys.argv) != 2:
		print "You must enter a message!"
		sys.exit()
	
	TelegramNotifier().sendMessageToAll(sys.argv[1])
