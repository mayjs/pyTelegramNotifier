import telegram
import json

with open("config.json") as config_file:
	config = json.load(config_file)
	bot = telegram.Bot(token=config["bot"]["token"])
	print '\n'.join(set(["%s %s: Chat-ID %d"%(update.message.from_user.first_name,update.message.from_user.last_name,update.message.chat.id) for update in bot.getUpdates()]))

