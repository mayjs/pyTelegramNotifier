# Python Telegram Notfiers
This tool allows you to send notifications to telegram chats.  
It loads its config from the given telegram_config.json file which contains your Telegram access information and all chat ids.  
By running "python showChats.py" you can get a list of all currently active chats of your telegram bot with the needed chatid for your config.  

To use this script, you must install the telegram bot api via pip, which can be done by executing the install.sh script.
